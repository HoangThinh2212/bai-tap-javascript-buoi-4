// Câu 1

/*
Input: 3 số nguyên

Step:
Step 1. Tạo ra 3 biến lưu trữ 3 số nguyên để người dùng nhập vào từ bàn phím
Step 2. so sánh lần lượt từng số với nhau
Step 3. sắp xếp thứ tự
Step 4. Xuất ra 3 số theo thứ tự tăng dần

Output: 3 số nguyên được sắp xếp theo thứ tự tăng dần

*/
function sapXep() {
  var so1El = document.getElementById("sot1").value * 1;
  var so2El = document.getElementById("sot2").value * 1;
  var so3El = document.getElementById("sot3").value * 1;

  if (so1El > so2El && so1El > so3El) {
    if (so2El > so3El) {
      document.getElementById("result1").innerHTML =
        so3El + " , " + so2El + " , " + so1El;
    } else {
      document.getElementById("result1").innerHTML =
        so2El + " , " + so3El + " , " + so1El;
    }
  } else if (so1El < so2El && so2El > so3El) {
    if (so1El > so3El) {
      document.getElementById("result1").innerHTML =
        so3El + " , " + so1El + " , " + so2El;
    } else {
      document.getElementById("result1").innerHTML =
        so1El + " , " + so3El + " , " + so2El;
    }
  } else if (so3El > so1El && so3El > so2El) {
    if (so1El > so2El) {
      document.getElementById("result1").innerHTML =
        so2El + " , " + so1El + " , " + so3El;
    } else {
      document.getElementById("result1").innerHTML =
        so1El + " , " + so2El + " , " + so3El;
    }
  }
}

// Câu 2
/* 
Input: Giá trị khi người dùng chọn trên dropdown

Step
Step 1. Tạo ra danh sách các giá trị để người dùng chọn
Step 2. Lấy giá trị đó và xuất ra khi người dùng ấn vào nút gửi lời chào

Output: Lời chào với giá trị người dùng vừa chọn
*/
function greetingMember() {
  var memEl = document.getElementById("txt-thanh-vien").value;
  document.getElementById(
    "result2"
  ).innerHTML = `<p>Xin chào <span>${memEl}</span></p>`;
}

// Câu 3
/*
Input: 3 số ngẫu nhiên

Step:
Step 1. Tạo ra 3 biến lưu trữ 3 số người dùng vừa nhập
Step 2. Thực hiện tìm số chẵn số lẻ trong 3 số đó
Step 3. Tạo ra 1 biến đếm để xác định số lượng số chẵn số lẻ có trong 3 số vừa nhập
Step 4. Xuất ra màn hình cho người dùng thấy

Output:
Số lượng số chẵn số lẻ có trong 3 số vừa nhập

*/
function tinhChanLe() {
  var so1El = document.getElementById("so1").value * 1;
  var so2El = document.getElementById("so2").value * 1;
  var so3El = document.getElementById("so3").value * 1;
  var soLuongSoChan = 0,
    soLuongSoLe = 0;

  if (so1El % 2 == 0) {
    soLuongSoChan++;
  }
  if (so2El % 2 == 0) {
    soLuongSoChan += 1;
  }
  if (so3El % 2 == 0) {
    soLuongSoChan += 1;
  }

  soLuongSoLe = 3 - soLuongSoChan;
  document.getElementById(
    "result3"
  ).innerHTML = `<p>Số chẵn: ${soLuongSoChan} ; Số lẻ: ${soLuongSoLe}</p>`;
}

// Câu 4
/* 
Input: 3 cạnh của 1 tam giác

Step:
Step 1. Tạo ra 3 biến lưu trữ 3 cạnh bất kì mà người dùng nhập vào
Step 2. Tính toán và So sánh 3 biến đó để dự đoán tam giác
Step 3. Xuất ra màn hình kết quả dự đoán

Output: Kết quả phép dự đoán tam giác

*/
function duDoan() {
  var canh1 = document.getElementById("canh1").value * 1;
  var canh2 = document.getElementById("canh2").value * 1;
  var canh3 = document.getElementById("canh3").value * 1;
  // console.log("canh1: ", canh1);
  // console.log("canh2: ", canh2);
  // console.log("canh3: ", canh3);

  if (
    canh1 + canh2 > canh3 &&
    canh1 + canh3 > canh2 &&
    canh2 + canh3 > canh1 &&
    canh1 > 0 &&
    canh2 > 0 &&
    canh3 > 0
  ) {
    // Xét tam giác đều
    if (canh1 == canh2 && canh2 == canh3) {
      document.getElementById("result4").innerHTML = `<p>Hình tam giác đều</p>`;
    }

    // Xét tam giác cân
    else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
      document.getElementById("result4").innerHTML = `<p>Hình tam giác cân</p>`;
    }

    // Xét tam giác vuông
    else if (
      canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
      canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
      canh3 * canh3 == canh1 * canh1 + canh2 * canh2
    ) {
      document.getElementById(
        "result4"
      ).innerHTML = `<p>Hình tam giác vuông</p>`;
    } else {
      document.getElementById(
        "result4"
      ).innerHTML = `<p>Hình tam giác khác</p>`;
    }
  } else {
    alert("Dữ liệu không hợp lệ!");
  }
}

// Câu 5
/* 
Input: ngày mong muốn 

Step
Step 1. Tạo ra 3 biến lưu trữ ngày , tháng , năm mà người dùng sẽ nhập
Step 2. Kiểm tra năm vừa nhập có phải là năm nhuận hay không?
Step 3. Kiểm tra tháng vừa nhập
Step 4. Kiểm tra ngày vừa nhập
Step 5. Thực hiện tính để lấy được ngày mai và ngày hôm qua

Output: Ngày tháng năm lấy được từ ngày tháng năm đã nhập
*/
// function kiemTraNgay() {
//   var ngayNhap = document.getElementById("txt-ngay").value * 1;
//   var thangNhap = document.getElementById("txt-thang").value * 1;
//   var namNhap = document.getElementById("txt-nam").value * 1;

//   ngayNhap = new Date();
//   ngayNhap.getDate();
// }

//Câu 7
/* 
Input: nhập số 3 chữ số
Step
Step 1. Tạo biến lưu trữ số có 3 chữ số vừa nhập
Step 2. Tách số trên thành các phần nhỏ như hàng trăm , hàng chục và hàng đơn vị
Step 3. dùng hàm switch case để đọc số đó 
Step 4. Xuất ra màn hình

Output: Cách đọc số vừa nhập
*/

function docSo() {
  var soNhap = document.getElementById("so3ChuSo").value * 1;
  var soHangTram, soHangChuc, soDonVi;

  soHangTram = Math.floor(soNhap / 100);
  soHangChuc = Math.floor((soNhap / 10) % 10);
  soDonVi = soNhap % 10;
  console.log("soHangTram: ", soHangTram);
  console.log("soHangChuc: ", soHangChuc);
  console.log("soDonVi: ", soDonVi);
  if (soHangTram < 0) {
    alert("Dữ liệu không hợp lệ");
  }
  switch (soHangTram) {
    case 1:
      document.getElementById("result7").innerHTML = "Một trăm ";
      break;
    case 2:
      document.getElementById("result7").innerHTML = "Hai trăm ";
      break;
    case 3:
      document.getElementById("result7").innerHTML = "Ba trăm";
      break;
    case 4:
      document.getElementById("result7").innerHTML = "Bốn trăm";
      break;
    case 5:
      document.getElementById("result7").innerHTML = "Năm trăm";
      break;
    case 6:
      document.getElementById("result7").innerHTML = "Sáu trăm";
      break;
    case 7:
      document.getElementById("result7").innerHTML = "Bảy trăm";
      break;
    case 8:
      document.getElementById("result7").innerHTML = "Tám trăm";
      break;
    case 9:
      document.getElementById("result7").innerHTML = "Chín trăm";
      break;

    default:
      alert("Dữ liệu không hợp lệ");
      break;
  }

  // làm sẵn số hàng chục và số đơn vị
  switch (soHangChuc) {
    case 0:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " lẻ ");
      switch (soDonVi) {
        case 0:
          switch (soHangTram) {
            case 1:
              document.getElementById("result7").innerHTML = "Một trăm ";
              break;
            case 2:
              document.getElementById("result7").innerHTML = "Hai trăm ";
              break;
            case 3:
              document.getElementById("result7").innerHTML = `<p>Ba trăm </p>`;
              break;
            case 4:
              document.getElementById("result7").innerHTML = `<p>Bốn trăm </p>`;
              break;
            case 5:
              document.getElementById("result7").innerHTML = `<p>Năm trăm </p>`;
              break;
            case 6:
              document.getElementById("result7").innerHTML = `<p>Sáu trăm </p>`;
              break;
            case 7:
              document.getElementById("result7").innerHTML = `<p>Bảy trăm </p>`;
              break;
            case 8:
              document.getElementById("result7").innerHTML = `<p>Tám trăm </p>`;
              break;
            case 9:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm </p>`;
              break;

            default:
              alert("Dữ liệu không hợp lệ");
              break;
          }
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín");
          break;

        default:
          break;
      }
      break;
    case 1:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " mười ");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín");
          break;

        default:
          break;
      }
      break;
    case 2:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " hai mươi ");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín");
          break;

        default:
          break;
      }
      break;
    case 3:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " ba mươi ");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín");
          break;

        default:
          break;
      }
      break;
    case 4:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " bốn mươi ");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba ");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn ");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm ");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu ");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy ");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám ");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín ");
          break;

        default:
          break;
      }
      break;
    case 5:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " năm mươi");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba ");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn ");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm ");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu ");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy ");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám ");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín ");
          break;

        default:
          break;
      }
      break;
    case 6:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " sáu mươi");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba ");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn ");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm ");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu ");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy ");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám ");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín ");
          break;

        default:
          break;
      }
      break;
    case 7:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " bảy mươi");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba ");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn ");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm ");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu ");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy ");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám ");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín ");
          break;

        default:
          break;
      }
      break;
    case 8:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " tám mươi");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba ");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn ");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm ");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu ");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy ");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám ");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín ");
          break;

        default:
          break;
      }
      break;
    case 9:
      document
        .getElementById("result7")
        .insertAdjacentText("beforeend", " chín mươi");
      switch (soDonVi) {
        case 0:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", "");
          break;
        case 1:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " một ");
          break;
        case 2:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " hai ");
          break;
        case 3:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " ba ");
          break;
        case 4:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bốn ");
          break;
        case 5:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " năm ");
          break;
        case 6:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " sáu ");
          break;
        case 7:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " bảy ");
          break;
        case 8:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " tám ");
          break;
        case 9:
          document
            .getElementById("result7")
            .insertAdjacentText("beforeend", " chín ");
          break;

        default:
          break;
      }
      break;
    default:
      break;
  }
}

// Câu 8
/*
Input: Họ tên 3 sinh viên cùng tọa độ của từng học sinh, tọa độ của trường học

Step
Step 1. Tạo biến lưu trữ giá trị được người dùng nhập
Step 2. Thực hiện đo lường khoảng cách
Step 3. So sánh các khoảng cách đo được với nhau và tìm ra kết quả lớn nhất chính là kết quả cuối cùng
Step 4. xuất ra màn hình kết quả

Output: Họ tên sinh viên xa trường nhất

*/
function doLuong() {
  // Lấy họ tên hs
  var hs1 = document.getElementById("txt-ho-ten-1").value;
  var hs2 = document.getElementById("txt-ho-ten-2").value;
  var hs3 = document.getElementById("txt-ho-ten-3").value;
  hs1 = hs1*1;
  console.log('hs1: ', typeof hs1);

  // Tọa độ hs1
  var x1 = document.getElementById("locX-1").value;
  var y1 = document.getElementById("locY-1").value;

  // Tọa độ hs2
  var x2 = document.getElementById("locX-2").value;
  var y2 = document.getElementById("locY-2").value;

  // Tọa độ hs3
  var x3 = document.getElementById("locX-3").value;
  var y3 = document.getElementById("locY-3").value;

  // Tọa độ trường học
  var x4 = document.getElementById("locX-4").value;
  var y4 = document.getElementById("locY-4").value;

  if (
    // Kiểm tra nếu có bất kỳ mục nào người dùng chưa nhập giá trị vào
    hs1.length == 0 ||
    hs2.length == 0 ||
    hs3.length == 0 ||
    x1.length == 0 ||
    y1.length == 0 ||
    x2.length == 0 ||
    y2.length == 0 ||
    x3.length == 0 ||
    y3.length == 0 ||
    x4.length == 0 ||
    y4.length == 0
  ) {
    alert("Dữ liệu không hợp lệ");
    document.getElementById("result8").innerHTML = "";
  } else {
    //Nếu đã nhập, thì chuyển từ String thành Number và thực hiện so sánh như thường
    x1 = x1 * 1;
    y1 = y1 * 1;
    x2 = x2 * 1;
    y2 = y2 * 1;
    x3 = x3 * 1;
    y3 = y3 * 1;
    x4 = x4 * 1;
    y4 = y4 * 1;
    if (
      //Kiểm tra xem dữ liệu tọa độ nhập vào có < 0 ko
      x1 < 0 ||
      y1 < 0 ||
      x2 < 0 ||
      y2 < 0 ||
      x3 < 0 ||
      y3 < 0 ||
      x4 < 0 ||
      y4 < 0
    ) {
      alert("Dữ liệu không hợp lệ");
      document.getElementById("result8").innerHTML = "";
    } else {
      // Độ dài đoạn đường từng học sinh và trường
      var d1,
        d2,
        d3,
        // Tính toán độ dài
        d1 = Math.sqrt(Math.pow(x4 - x1, 2) + Math.pow(y4 - y1, 2));

      d2 = Math.sqrt(Math.pow(x4 - x2, 2) + Math.pow(y4 - y2, 2));

      d3 = Math.sqrt(Math.pow(x4 - x3, 2) + Math.pow(y4 - y3, 2));

      if (d1 > d2 && d1 > d3) {
        document.getElementById("result8").innerHTML =
          "Sinh viên xa trường nhất: " + hs1;
      } else if (d2 > d1 && d2 > d3) {
        document.getElementById("result8").innerHTML =
          "Sinh viên xa trường nhất: " + hs2;
      } else if (d3 > d1 && d3 > d2) {
        document.getElementById("result8").innerHTML =
          "Sinh viên xa trường nhất: " + hs3;
      }
    }
  }
}
